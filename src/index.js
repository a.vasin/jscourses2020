import { washingMachines } from "./data.js";

let productWashingMachine = "";
let productWashingMachineTitle = "";
let productWashingMachinePrice = "";

function viewWashingMachinesInConsole() {
	console.table(washingMachines);
}

function createWashingMachineTitle(name) {
	productWashingMachineTitle = document.createElement('h2');
	productWashingMachineTitle.append(name);
}

function createWashingMachinePrice(price, dataId) {
	productWashingMachinePrice = document.createElement('button');
	productWashingMachinePrice.setAttribute('class', 'productsWashingMachines-button');
	productWashingMachinePrice.setAttribute('data-id', dataId);
	productWashingMachinePrice.append(price, ' руб');
}

function createWashingMachine(title, price) {
	productWashingMachine = document.createElement('div');
	productWashingMachine.append(title, price);
}

viewWashingMachinesInConsole();

const promiseWashingMachinesData = new Promise(function(resolve, reject) {
	const rand = Math.random();
	if (rand < 0.9) {
		setTimeout(function() {
			if (rand > 0.1) {
				resolve(washingMachines);
			} else {
				reject('Unable to load the data. Please, try later.');
			}
		}, 2000);
	} else {
		throw new Error('Error during connection. Please, try again.');
	}
});

promiseWashingMachinesData.then(
	result => {
		const dataWashingMachines = result;
		const containerWashingMachines = document.getElementById('washingMachines');

		const productsWashingMachines = dataWashingMachines.map(({ name, price, volume, dataId }) => {
			createWashingMachineTitle(name);
			createWashingMachinePrice(price, dataId);
			createWashingMachine(productWashingMachineTitle, productWashingMachinePrice);

			return productWashingMachine;
		});

		containerWashingMachines.append(...productsWashingMachines);

		const buttonsWashingMachines = document.querySelectorAll('.productsWashingMachines-button');
		let cartNumber = document.getElementById('menu-cart').innerText;
		let productWashingMacninesIds = [];
		let indexWashingMachine = 0;

		buttonsWashingMachines.forEach(item => {
			item.addEventListener('click', event => {
				if (productWashingMacninesIds.includes(item.getAttribute('data-id'))) {
					indexWashingMachine = productWashingMacninesIds.indexOf(item.getAttribute('data-id'));
					if (indexWashingMachine > -1) {
						productWashingMacninesIds.splice(indexWashingMachine, 1);
					}
					cartNumber--;
				} else {
					productWashingMacninesIds.push(item.getAttribute('data-id'));
					cartNumber++;
				}
				document.getElementById('menu-cart').innerText = cartNumber;
			})
		});
	},
	error => {
		alert(error);
	}
).catch(error => {
	alert(error);
});