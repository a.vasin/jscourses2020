const path = require("path");
const CopyPlugin = require('copy-webpack-plugin');

const config = {
	entry: path.resolve(__dirname, "src", "index.js"),
	output: {
		filename: "bundle.js",
		publicPath: "/",
		path: path.resolve(__dirname, "dist")
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: ["style-loader", "css-loader"]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: ["file-loader"]
			}
		]
	},
	plugins: [
		new CopyPlugin({
			patterns: [
				{ from: path.resolve(__dirname, "src/css/"), to: path.resolve(__dirname, "dist/css/"), force: true },
				{ from: path.resolve(__dirname, "src/fonts/"), to: path.resolve(__dirname, "dist/fonts/"), force: true },
				{ from: path.resolve(__dirname, "src/images/"), to: path.resolve(__dirname, "dist/images/"), force: true },
				{ from: path.resolve(__dirname, "src/js/"), to: path.resolve(__dirname, "dist/js/"), force: true },
			],
			options: {
				concurrency: 100,
			},
		}),
	],
	devServer: {
		host: 'localhost',
		port: 8080,
		contentBase: path.join(__dirname, "dist"),
		hot: true,
	    open: true,
	    openPage: ''
	}
};

module.exports = config;